package com.example.securitydemo.config;

import java.util.Optional;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import com.example.securitydemo.domain.User;

public class AuditAwareImpl implements AuditorAware<String> {

  @Override
  public Optional<String> getCurrentAuditor() {
    Authentication sc = SecurityContextHolder.getContext().getAuthentication();

    Optional<String> user = Optional.empty();
    if (sc.isAuthenticated()) {
      Object principal = sc.getPrincipal();
      if (principal.getClass().equals(String.class)) {
        user = Optional.of(String.valueOf(principal));
      } else {
        user = Optional.of(((User) principal).getUsername());
      }
    }
    return user;
  }
}
