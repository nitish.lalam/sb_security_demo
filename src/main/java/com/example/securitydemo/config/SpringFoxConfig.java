package com.example.securitydemo.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SpringFoxConfig {                              
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          //.apis(RequestHandlerSelectors.any())
          .apis(RequestHandlerSelectors.basePackage("com.example.securitydemo.controller"))
          //.paths(PathSelectors.any())
          .paths(PathSelectors.ant("/securitydemo/**"))
          .build()
          .apiInfo(apiInfo());                                           
    }

	/*
	 * v2/api-docs or v3/api-docs for json response 
	 * 
	 * swagger-ui/ for UI
	 */

	private ApiInfo apiInfo() {
		return new ApiInfo(
			      "Security API Demo", //page heading
			      null,//"Some custom description of API.", 
			      "1.0.0", //app version
			      null, //terms of service url
			      null,//new Contact("Chakravarthi Lalam", "../home/index", "nitish.lalam@live.com"), 
			      null,//"License of API", 
			      null,//"../home/index", license url
			      Collections.emptyList());
	}
}