package com.example.securitydemo.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import liquibase.integration.spring.SpringLiquibase;

@Configuration
@EnableJpaRepositories("com.example.securitydemo.repository")
@EnableJpaAuditing(auditorAwareRef = "customAuditProvider")// --> to get current auditor, can create custom bean
@EnableTransactionManagement
public class DataSourceConfiguration {

  private final Environment env;

  public DataSourceConfiguration(Environment env) {
    this.env = env;
  }
  
  @Bean
  public AuditorAware<String> customAuditProvider() {
	  return new AuditAwareImpl();
  }

	/*
	 * to run liquibase use run as maven build -> 
	 * 1. liquibase:diff 
	 * 2. liquibase:generateChangeLog
	 */
  @Bean
  public SpringLiquibase liquibase(
      DataSource dataSource) {

    // Use liquibase.integration.spring.SpringLiquibase if you don't want Liquibase
    // to start asynchronously
    SpringLiquibase liquibase = new SpringLiquibase();
    liquibase.setDataSource(dataSource);
    liquibase.setChangeLog("classpath:db/changelog/mt_master.xml");
    if (env.acceptsProfiles("no-liquibase")) {
      liquibase.setShouldRun(false);
    } else {
      liquibase.setShouldRun(true);
    }
    return liquibase;
  }
}