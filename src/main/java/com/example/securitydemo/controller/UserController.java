package com.example.securitydemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.securitydemo.domain.User;
import com.example.securitydemo.service.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {

	@Autowired
	UserService userDetailsService;
	
	@GetMapping("/")
	public List<User> getUsers() {
		return userDetailsService.findAll();
	}
	
}
