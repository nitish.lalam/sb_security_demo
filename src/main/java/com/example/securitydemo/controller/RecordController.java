package com.example.securitydemo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.example.securitydemo.domain.Record;
import com.example.securitydemo.service.RecordService;

@RestController
@RequestMapping("/api/records")
public class RecordController {

	@Autowired
	RecordService recordService;
	
	@GetMapping("/create")
	public Record createRecord(@RequestParam("value") String value) {
		Record rec = new Record();
		rec.setValue(value);
		return recordService.save(rec);
	}
	
    @GetMapping({"", "/"})
	public List<Record> getRecords() {
		return recordService.getRecords();
	}
	
	@GetMapping("/{id}")
	public Record getRecord(@PathVariable Long id) {
		return recordService.getRecord(id);
	}
}
