package com.example.securitydemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.securitydemo.domain.User;
import com.example.securitydemo.service.UserService;

@RestController
@RequestMapping("/")
public class HomeController {

	@Autowired
	UserService userDetailsService;
	
	@GetMapping("index")
	public String getWelcomeMessage() {
		return "Hello World..!";
	}
	
	@GetMapping("register/{username}/{password}")
	public User registerUser(@PathVariable String username, @PathVariable String password) {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		return userDetailsService.save(user);
	}
	
}