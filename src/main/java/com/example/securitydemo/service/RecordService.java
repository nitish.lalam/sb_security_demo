package com.example.securitydemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.securitydemo.domain.Record;
import com.example.securitydemo.repository.RecordRepository;

@Service
public class RecordService {

	@Autowired
	RecordRepository recordRepository;
	
	public List<Record> getRecords() {
		return recordRepository.findAll();
	}
	
	public Record getRecord(Long id) {
		return recordRepository.findById(id).orElse(null);
	}
	
	public Record save(Record record) {
		return recordRepository.save(record);
	}
}
