package com.example.securitydemo.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.securitydemo.domain.User;
import com.example.securitydemo.domain.UserAuthority;
import com.example.securitydemo.repository.UserRepository;

@Service
@Transactional
public class UserService implements UserDetailsService {

	@Autowired
	UserRepository userRepository;

	@Transactional(readOnly = true)
	public Optional<User> findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Transactional(readOnly = true)
	public List<User> findAll() {
		return userRepository.getAllUsers();
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		return this.findByUsername(username)
				.map(MyUser::new)
				.orElseThrow(() -> new UsernameNotFoundException(username));
	}
	
	public User save(User user) {
		UserAuthority ua = new UserAuthority();
		ua.setAuthority("ROLE_USER");
		UserAuthority ua1 = new UserAuthority();
		ua1.setAuthority("ROLE_ADMIN");
		
		BCryptPasswordEncoder pe = new BCryptPasswordEncoder();
		user.setPassword(pe.encode(user.getPassword()));

		user.addToAutority(ua);
		user.addToAutority(ua1);

		return userRepository.save(user);
	}

	private static class MyUser extends User implements UserDetails {

		public MyUser(User user) {
			super(user);
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			return super.getUserAuthorities()
					.stream()
					.map(ua -> new SimpleGrantedAuthority(ua.getAuthority()))
					.collect(Collectors.toList());
		}

		@Override
		public boolean isAccountNonExpired() {
			return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return true;
		}

		@Override
		public boolean isEnabled() {
			return super.getEnabled();
		}

	}
}
