package com.example.securitydemo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.securitydemo.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

	@EntityGraph(attributePaths = {"userAuthorities"})
	Optional<User> findByUsername(String username);
	
	@Query("select distinct u from User u left join fetch u.userAuthorities")
	List<User> getAllUsers();
	
}
