package com.example.securitydemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.securitydemo.domain.Record;

@Repository
public interface RecordRepository extends JpaRepository<Record, Long>{

	
}
